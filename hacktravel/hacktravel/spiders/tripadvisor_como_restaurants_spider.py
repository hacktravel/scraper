import scrapy


class TripAdvisorComoRestaurantsSpider(scrapy.Spider):
    name = 'tripadvisor_como_restaurants'
    start_urls = open('como_restaurants_next_page_links','r').read().split('\n')

    def parse(self, response):
        restaurant_links = response.css('.property_title::attr(href)')
        for restaurant_link in restaurant_links:
            restaurant_link = response.urljoin(restaurant_link.extract())
            print('restaurant_link: %s' % restaurant_link)
            open('./como_restaurants_urls','a').write('%s\n' % restaurant_link)
        return
