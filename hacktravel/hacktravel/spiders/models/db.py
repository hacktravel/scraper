import mongokit
import datetime

from configs import Configs


class Datable(mongokit.Document):
    structure = {
        'created_at':   datetime.datetime,
        'updated_at':   datetime.datetime
    }

    default_value = {
        'created_at':   datetime.datetime.utcnow()
    }

class User(Datable):
    structure = {
        'first_name': basestring,
        'last_name': basestring,
        'email': basestring,
        'password': basestring,
        'keywords': [basestring],
    }

    required_fields = ['email', 'password']

    default_value = {
        'first_name': '',
        'last_name': '',
        'keywords': []
    }


class Location(Datable):
    structure = {
        'country': basestring,
        'state/province': basestring,
        'city': basestring,
        'zip_code': basestring,
        'address': basestring,
        'lat': float,
        'log': float,
        'type': basestring,
        'name': basestring,
        'email': basestring,
        'phone': basestring,
        'website': basestring,
        'reviews': [mongokit.ObjectId],
        'sentiment': float,
        'keywords': float,
    }


class Review(Datable):
    structure = {
        'url': basestring,
        'title': basestring,
        'text': basestring,
    }


class DB(object):
    def __init__(self):

        connection = mongokit.Connection(
            Configs().JSON['persistence']['mongodb']['url']
        )

        connection.register([
            User,
            Location,
            Review
        ])

        self.connection = connection
