import json


class Configs(object):
    def __init__(self):
        self.json_string = open('configs.json', 'r').read()
        self.JSON = json.loads(self.json_string)