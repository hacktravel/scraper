import scrapy


class TripAdvisorComoActivitiessSpider(scrapy.Spider):
    name = 'tripadvisor_como_activities'
    start_urls = open('como_activities_next_page_links','r').read().split('\n')

    def parse(self, response):
        activity_links = response.css('.property_title > a::attr(href)')
        for activity_link in activity_links:
            activity_link = response.urljoin(activity_link.extract())
            print('activity_link: %s' % activity_link)
            open('./como_activities_urls','a').write('%s\n' % activity_link)
        return
