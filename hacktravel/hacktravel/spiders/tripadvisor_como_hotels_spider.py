import scrapy


class TripAdvisorComoHotelsSpider(scrapy.Spider):
    name = 'tripadvisor_como_hotels'
    start_urls = open('como_hotels_next_page_links','r').read().split('\n')

    def parse(self, response):
        hotel_links = response.css('.property_title::attr(href)')
        for hotel_link in hotel_links:
            hotel_link = response.urljoin(hotel_link.extract())
            print('hotel_link: %s' % hotel_link)
            open('./como_hotels_urls','a').write('%s\n' % hotel_link)
        return
