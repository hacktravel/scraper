import scrapy


class TripAdvisorComoVacationRentalsSpider(scrapy.Spider):
    name = 'tripadvisor_como_vacation_rentals_urls'
    start_urls = ['http://www.tripadvisor.it/VacationRentals-g187835-Reviews-Como_Lake_Como_Lombardy-Vacation_Rentals.html']
    open('./como_vacation_rentals_next_page_links','a').write('%s\n' % start_urls[0])


    def parse(self, response):
        next_page_link = response.css('.nextArrow::attr(href)')
        if next_page_link:
            next_page_link = response.urljoin(next_page_link.extract()[0])
            print('next_page_link: %s' % next_page_link)
            open('./como_vacation_rentals_next_page_links','a').write('%s\n' % next_page_link)
            yield scrapy.Request(next_page_link, callback=self.parse)
        else:
            return
