import scrapy


class TripAdvisorComoRestaurantsSpiderSpider(scrapy.Spider):
    name = 'tripadvisor_como_activities_urls'
    start_urls = ['http://www.tripadvisor.it/Attractions-g187835-Activities-Como_Lake_Como_Lombardy.html']
    open('./como_activities_next_page_links','a').write('%s\n' % start_urls[0])


    def parse(self, response):
        next_page_link = response.css('.sprite-pageNext::attr(href)')
        if next_page_link:
            next_page_link = response.urljoin(next_page_link.extract()[0])
            print('next_page_link: %s' % next_page_link)
            open('./como_activities_next_page_links','a').write('%s\n' % next_page_link)
            yield scrapy.Request(next_page_link, callback=self.parse)
        else:
            return
