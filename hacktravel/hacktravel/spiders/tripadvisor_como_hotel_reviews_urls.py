import scrapy


class TripAdvisorComoReviewsSpider(scrapy.Spider):
    name = 'tripadvisor_como_hotel_reviews'
    start_urls = open('como_hotel_next_page_links','r').read().split('\n')

    def parse(self, response):
        review_links = response.css('.quote > a::attr(href)')
        for review_link in review_links:
            review_link = response.urljoin(review_link.extract())
            print('review_link: %s' % review_link)
            open('como_hotel_reviews_urls','a').write('%s\n' % review_link)
        return
