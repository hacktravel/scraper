import scrapy


class TripAdvisorComoActivitySpider(scrapy.Spider):
    name = 'tripadvisor_como_activity_urls'
    start_urls = open('./como_activities_urls','r').read().split('\n')


    def parse(self, response):
        open('./como_activity_next_page_links','a').write('%s\n' % response.url)
        next_page_link = response.css('.next::attr(href)')
        if next_page_link:
            next_page_link = response.urljoin(next_page_link.extract()[0])
            print('next_page_link: %s' % next_page_link)
            open('./como_activity_next_page_links','a').write('%s\n' % next_page_link)
            yield scrapy.Request(next_page_link, callback=self.parse)
        else:
            return

