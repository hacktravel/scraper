from __future__ import unicode_literals
import scrapy
import re
from dragnet import content_comments_extractor
from nltk.corpus import stopwords
import datetime


def filter_link(link, kind):
    pattern = ''

    if kind == 'reviews':
        pattern = re.compile('/((\w+)(-)?)*\.(html)')
    elif kind == 'review':
        pattern = re.compile('/(ShowUserReviews-)((\w+)(-)?)*\.(html)(#CHECK_RATES_CONT)')
    else:
        return False

    if link:
        if pattern.match(link):
            return True
        else:
            return False
    else:
        return False


def extract_text(html):
    text = content_comments_extractor.analyze(html)
    return text


def preprocess_text(text):
    letters_only = re.sub("[^a-zA-Z]",
                          " ",
                          text)
    lower_case = letters_only.lower()
    words = lower_case.split()
    words = [w for w in words if w not in stopwords.words('english')]
    preprocessed_text = " ".join(words)
    return preprocessed_text


def extract_hotel_name(response):
    hotel_name = response.css('#HEADING::text').extract()[0]
    hotel_name = hotel_name
    return hotel_name

def clean_city(city):
    # example: '@ Milano (MI)'
    city = city.replace('@ ','')
    return city

def extract_phone_and_email(referente):
    contacts = referente.split('<br>')
    phone = contacts[2].replace('</div>','').rstrip()
    email = ''
    if len(contacts) > 3:
        email = contacts[3].replace('</div>','').rstrip()
    res = {
        'phone': phone,
        'email': email
    }
    return res

def extract_disco(response):
    name = response.css('.titolo > a::text').extract()[0]
    city = response.css('.descrizione > strong::text').extract()[0]
    city = clean_city(city)
    referente = response.css('.referente-disco').extract()[0]
    contacts = extract_phone_and_email(referente)
    phone = contacts['phone']
    email = contacts['email']
    disco = {
        'name': name,
        'city': city,
        'phone': phone,
        'email': email
    }
    return disco

def discos(response):
    discos_raw = response.css('.elenco > ul > li')
    discos = map(lambda x: extract_disco(x), discos_raw)
    return discos


class  DiscotecheSpider(scrapy.Spider):
    name = 'discoteche'
    start_urls = ['http://www.angelipierre.net/discoteche.php?id_localita=12']

    def parse(self, response):
        next_link_href = response.css('.nextlink a::attr(href)')
        # next_link = response.urljoin(next_link_href.extract())
        print(next_link_href)
        # discs = discos(response)
        # print('discos: %s', discs)
        # yield scrapy.Request(next_link, callback=self.parse)
